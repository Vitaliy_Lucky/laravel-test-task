<?php

return [
    'max_items' => env('INQUIRE_MAX_ITEMS', 2000),

    "failed_jobs_percent" => env('ITEM_JOBS_FAILS_PERCENT', 30),

    // in seconds
    "delay_jobs" => env('ITEM_JOBS_SLEEP', 5),
];
