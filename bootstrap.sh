#!/bin/bash

set -e

sudo chmod -R 755 storage ./bootstrap/cache

if [ ! -f .env ]; then
  cp .env.example .env

  echo "WWWUSER=$(id -u)"  >> .env
  echo "WWWGROUP=$(id -g)"  >> .env

  # install composer dependencies
  docker compose run --entrypoint= --rm --no-deps app composer install

  # generating the APP_KEYS
  docker compose run --entrypoint= --rm --no-deps app php artisan key:generate
  docker compose run --entrypoint= --rm app php artisan migrate
else
  docker compose run --entrypoint= --rm  --no-deps app composer install
fi

docker compose up -d app queue

echo "The application available by http://10.3.0.1"

browse http://10.3.0.1


