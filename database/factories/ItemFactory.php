<?php

namespace Database\Factories;

use App\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    protected $model = Item::class;


    public function definition(): array
    {
        return [
            'ref' => fake()->unique()->regexify('[A-Z]-[0-9]{1,10}'),
            'name'=> fake()->words(3, true),
            'description' => fake()->text(100),
        ];
    }
}
