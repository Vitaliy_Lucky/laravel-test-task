<?php

namespace Tests\Unit\Models;

use App\Enums\ItemStatus;
use App\Models\Item;
use Tests\TestCase;

class ItemTest extends TestCase
{
    /**
     * Status attribute test
     */
    public function test_status_attribute(): void
    {
        $item = new Item();
        $item->status = ItemStatus::NEW;
        $this->assertEquals(ItemStatus::NEW->value, $item->getAttributes()['status'],
            'The value of attribute should be the same as the value of Enum\'s case');
    }


    /**
     * Active scope test
     */
    public function test_active_scope(): void
    {
        $expectingWhere = [
            "type" => "Basic",
            "column" => "is_active",
            "operator" => "=",
            "value" => true,
            "boolean" => "and",
        ];

        // searching the expecting where on the wheres of query
        $hasWhereOnQuery = collect(Item::active()->getQuery()->wheres)
            ->contains(fn($where) => $expectingWhere == $where);

        $this->assertTrue($hasWhereOnQuery,
            'Wheres property of query must contain the filter by is_active field');
    }


    /**
     * Inactive scope test
     */
    public function test_inactive_scope(): void
    {
        $expectingWhere = [
            "type" => "Basic",
            "column" => "is_active",
            "operator" => "=",
            "value" => false,
            "boolean" => "and",
        ];

        // searching the expecting where on the wheres of query
        $hasWhereOnQuery = collect(Item::inactive()->getQuery()->wheres)
            ->contains(fn($where) => $expectingWhere == $where);

        $this->assertTrue($hasWhereOnQuery,
            'Wheres property of query must contain the filter by is_active field');
    }
}
