<?php

namespace Tests\Unit\Models;

use App\Enums\InquiryStatus;
use App\Models\Inquiry;
use PHPUnit\Framework\TestCase;

class InquiryTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    public function test_status_attribute(): void
    {
        $inquire = new Inquiry;
        $inquire->status = InquiryStatus::ACTIVE;
        $this->assertEquals(InquiryStatus::ACTIVE->value, $inquire->getAttributes()['status'],
            'The value of attribute should be the same as the value of Enum\'s case');
    }
}
