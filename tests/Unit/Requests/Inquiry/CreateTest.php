<?php

namespace Tests\Unit\Requests\Inquiry;

use App\Http\Requests\Inquiry\Create;
use Tests\TestCase;

class CreateTest extends TestCase
{
    public function test_rules()
    {
        $request = new Create([]);
        $rules = $request->rules();
        $this->assertIsArray($rules);
    }
}
