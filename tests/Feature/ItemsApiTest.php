<?php

namespace Tests\Feature;

use App\Jobs\{ProcessInquiryJob, ProcessItemJob};
use App\Models\{Inquiry, Item};
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class ItemsApiTest extends TestCase
{
    use DatabaseTransactions;

    public function test_item_activate()
    {
        $item = Item::factory()->create();

        $response = $this->put("api/items/{$item->ref}/activate");
        $response->assertStatus(422);

        $response = $this->put("api/items/{$item->ref}/deactivate");
        $response->assertStatus(200);
        $this->assertEquals($response->json('is_active'), false, 'The item must be deactivated');

        $response = $this->put("api/items/{$item->ref}/activate");
        $response->assertStatus(200);
        $this->assertEquals($response->json('is_active'), true, 'The item must be activated');

    }
}
