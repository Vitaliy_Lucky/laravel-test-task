<?php

namespace Tests\Feature;

use App\Enums\ItemStatus;
use App\Jobs\{ProcessInquiryJob, ProcessItemJob};
use App\Models\{Inquiry, Item};
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class InquireApiTest extends TestCase
{
    use DatabaseTransactions;

    public function test_inquire_description()
    {
        $items = [
            [
                "ref" => "T-1",
                "name" => "test",
                "description" => null,
            ],
            [
                "ref" => "T-2",
                "name" => "test",
                "description" => "Test description",
            ],
        ];
        $response = $this->post('api/demo/test', ['data' => $items]);
        $response->assertStatus(200);
    }

    public function test_inquire_max_dispatched()
    {
        Queue::fake();
        config(['inquiry.max_items' => 100]);

        $items = Item::factory()->count(101)->make()->toArray();

        $response = $this->post('api/demo/test', ['data' => $items]);
        $response->assertStatus(422);
    }


    public function test_inquire_post()
    {
        Queue::fake();
        $itemsCount = 5;
        $items = Item::factory()->count($itemsCount)->make()->toArray();

        $response = $this->post('api/demo/test', ['data' => $items]);
        $this->assertEquals($response->status(), 200, $response->json('message'));

        if ($response->isOk()) {
            $json = $response->json();
            $this->assertArrayHasKey('id', $json,
                'Response must return the inquiry\'s id: '.$response->getContent());
            $dispatchedJob = null;

            if (!empty($json['id'])) {
                $inquiry = Inquiry::findOrFail($json['id']);
                $this->assertEquals($itemsCount, $inquiry->items_total_count, 'Wrong number of items count');

                Queue::assertPushed(function (ProcessInquiryJob $job) use (&$dispatchedJob, $json) {
                    $dispatchedJob = ($job->inquiryId === $json['id']) ? $job : null;

                    return (bool) $dispatchedJob;
                });
            }

            if ($dispatchedJob) {
                $dispatchedJob->handle();

                Queue::assertPushed(ProcessItemJob::class, $itemsCount);
            } else {
                $this->assertTrue(false, 'Dispatched job is not processed');
            }
        }
    }


    public function test_inquire_update_items()
    {
        $items = Item::factory()->count(2)->create();

        $response = $this->post('api/demo/test', ['data' => $items->toArray()]);
        $response->assertStatus(200, $response->getContent());

        // deactivating the first item
        $item = $items->first();
        $item->is_active = false;
        $item->save();

        $response = $this->post('api/demo/test', ['data' => $items->toArray()]);
        $response->assertStatus(422, 'Updating inactive items must be forbidden');

        // First item to create, the second to update test
        $items[0] = Item::factory()->make();
        // Updating the field on second item
        $items[1]->name = 'test name';
        $response = $this->post('api/demo/test', ['data' => $items->toArray()]);
        $response->assertStatus(200);

        $itemModels = Item::whereIn('ref', $items->pluck('ref')->toArray())->get()->keyBy('ref');
        $refs = $items->pluck('ref');

        $this->assertEquals(
            ItemStatus::NEW,
            $itemModels[$refs[0]]->status,
            'Just created item must has a status NEW'
        );
        $this->assertEquals(
            ItemStatus::UPDATED,
            $itemModels[$refs[1]]->status,
            'Updated item must has a status UPDATED'
        );
    }
}
