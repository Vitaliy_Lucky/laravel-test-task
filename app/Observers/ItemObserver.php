<?php

namespace App\Observers;

use App\Enums\ItemStatus;
use App\Models\Item;

class ItemObserver
{
    /**
     * Handle the Item "creating" event.
     */
    public function creating(Item $item): void
    {
        if($item->is_active === null) {
            $item->is_active = true;
        }
    }


    /**
     * Handle the Item "created" event.
     */
    public function created(Item $item): void
    {
    }


    /**
     * Handle the Item "updated" event.
     */
    public function updating(Item $item): void
    {
        $item->status = ItemStatus::UPDATED;
    }


    /**
     * Handle the Item "updated" event.
     */
    public function updated(Item $item): void
    {
        //
    }


    /**
     * Handle the Item "deleted" event.
     */
    public function deleted(Item $item): void
    {
        //
    }


    /**
     * Handle the Item "restored" event.
     */
    public function restored(Item $item): void
    {
        //
    }


    /**
     * Handle the Item "force deleted" event.
     */
    public function forceDeleted(Item $item): void
    {
        //
    }
}
