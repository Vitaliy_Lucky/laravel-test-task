<?php

namespace App\Observers;

use App\Jobs\ProcessInquiryJob;
use App\Models\Inquiry;

/**
 * Class InquiryObserver
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Observers
 */
class InquiryObserver
{
    /**
     * Handle the Inquiry "created" event.
     */
    public function created(Inquiry $inquiry): void
    {
        ProcessInquiryJob::dispatch($inquiry->id);
    }


    /**
     * Handle the Inquiry "updated" event.
     */
    public function updating(Inquiry $inquiry): void
    {
        $inquiry->updateStatus();
    }


    /**
     * Handle the Inquiry "updated" event.
     */
    public function updated(Inquiry $inquiry): void
    {
        //
    }


    /**
     * Handle the Inquiry "deleted" event.
     */
    public function deleted(Inquiry $inquiry): void
    {
        //
    }


    /**
     * Handle the Inquiry "restored" event.
     */
    public function restored(Inquiry $inquiry): void
    {
        //
    }


    /**
     * Handle the Inquiry "force deleted" event.
     */
    public function forceDeleted(Inquiry $inquiry): void
    {
        //
    }
}
