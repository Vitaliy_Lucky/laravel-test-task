<?php

namespace App\Jobs;

use App\Enums\InquiryStatus;
use App\Models\{Inquiry};
use Illuminate\Bus\Batch;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\{InteractsWithQueue, SerializesModels};
use Throwable;

/**
 * Class ProcessInquiryJob
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Jobs
 */
class ProcessInquiryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @param  int  $inquiryId
     */
    public function __construct(public readonly int $inquiryId)
    {
    }


    /**
     * @return void
     */
    public function handle(): void
    {
        /** @var Inquiry $inquiry */
        $inquiry = Inquiry::findOrFail($this->inquiryId);
        $inquiry->status = InquiryStatus::ACTIVE;
        $inquiry->items_processed_count = 0;
        $inquiry->items_failed_count = 0;
        $inquiry->save();

        \Log::info("Processing Inquiry {$inquiry->id}");

        $jobs = $inquiry->payload
            ?->map(fn(array $itemData) => new ProcessItemJob($itemData, $inquiry->id));

        \Bus::batch($jobs)
            ->catch(function (Batch $batch, Throwable $e) use ($inquiry) {
                $inquiry->fresh();
                $inquiry->status = InquiryStatus::FAILED;
                $inquiry->syncWithBatch($batch);

                \Log::error('Error :'.$e);
            })
            ->then(function (Batch $batch) use ($inquiry) {
                $inquiry->fresh();
                $inquiry->status = InquiryStatus::PROCESSED;
                $inquiry->syncWithBatch($batch);

                \Log::info('Batch completed successfull');
            })
            ->finally(function (Batch $batch) use ($inquiry) {
                $inquiry->fresh();
                $inquiry->syncWithBatch($batch);

                \Log::info('Batch completed with result :', $inquiry->toArray());
            })
            ->allowFailures()
            ->dispatch();
    }
}
