<?php

namespace App\Jobs;

use App\Models\Inquiry;
use App\Models\Item;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class ProcessItemJob
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Jobs
 */
class ProcessItemJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;

    /**
     * @var int
     */
    public int $tries = 2;


    /**
     * @param  array  $itemData
     * @param  int    $inquiryId
     */
    public function __construct(private readonly array $itemData, private readonly int $inquiryId)
    {
    }


    /**
     * @return void
     * @throws \Exception
     */
    public function handle(): void
    {
        $this->updateInquiryStatus();
        /** @var Item $item */
        $item = Item::updateOrCreate(['ref' => $this->itemData['ref']], $this->itemData);
        \Log::info("Job ID: ".$this->job->getJobId().' Retry: '.$this->attempts(), $item->toArray());

        $this->probabilityFail();

        // incrementing the field on db level to prevent deadlock issue on multiple concurrent jobs
        $this->updateInquiryStatus();

        sleep(config('inquiry.delay_jobs', 0));
        \Log::info('Job processed '.$this->job->getJobId());
    }


    /**
     * @return void
     * @throws \Exception
     */
    private function probabilityFail()
    {
        // $digitalHash = crc32($this->job->getJobId()).'';
        $digitalHash = rand();
        // generate two digits hash for job - number from 0-99
        $digits = intval(Str::substr($digitalHash, strlen($digitalHash) - 2, strlen($digitalHash)));

        if ($digits < (config('inquiry.failed_jobs_percent')) - 1) {
            throw new \Exception('The expected fail '.$this->job->getJobId());
        }
    }


    /**
     * Handle a job failure.
     * This method is not
     */
    public function failed(Throwable $exception): void
    {
        \Log::error('Job  attempt: '.$this->attempts().' failed with message: '
            .$exception->getMessage());

        $this->updateInquiryStatus();
    }


    /**
     * @return void
     */
    private function updateInquiryStatus()
    {
        $inquiry = Inquiry::find($this->inquiryId);
        $batch = $this->batch();
        ($batch && $inquiry->syncWithBatch($batch)) || $inquiry->updateStatus()->save();
    }
}
