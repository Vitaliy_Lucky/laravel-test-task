<?php

namespace App\Http\Requests\Inquiry;

use App\Models\Item;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class Create
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Http\Requests
 */
class Create extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        $maxItemsAccepted = config('inquiry.max_items');

        return [
            'data' => "required|array|max:$maxItemsAccepted",
            'data.*.ref' => [
                "required",
                Rule::unique(Item::class, 'ref')->where('is_active', false),
            ],
            'data.*.name' => 'required|string|max:255',
            'data.*.description' => 'nullable|string|max:65535',
        ];
    }


    /**
     * @return true
     */
    public function authorize()
    {
        // TODO: implement the authorize method
        return true;
    }
}
