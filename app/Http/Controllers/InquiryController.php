<?php

namespace App\Http\Controllers;

use App\Enums\InquiryStatus;
use App\Http\Requests\Inquiry\Create;
use App\Models\Inquiry;

/**
 * Class InquiryController
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Http\Controllers
 */
class InquiryController extends Controller
{
    /**
     * @param  Create   $request
     * @param  Inquiry  $inquiry
     *
     * @return array
     */
    public function store(Create $request, Inquiry $inquiry)
    {
        $payload = collect($request->validated('data'));

        $inquiry->fill([
            'payload' => $payload,
            'status' => InquiryStatus::ACTIVE,
            'items_total_count' => $payload->count(),
        ]);

        $inquiry->save();

        return [
            'id' => $inquiry->id,
            'message' => 'Inquiry has been registered',
        ];
    }
}
