<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;

/**
 * Class ItemController
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Http\Controllers
 */
class ItemController extends Controller
{
    const STATUS_ACTIONS = [
        'activate' => true,
        'deactivate' => false,
    ];


    /**
     * @param  Request  $request
     * @param  Item     $item
     * @param  string   $action
     *
     * @return Item
     */
    public function changeStatus(Request $request, Item $item, string $action)
    {
        $desiredStatus = self::STATUS_ACTIONS[$action];

        if ($item->is_active == $desiredStatus) {
            abort(422, 'Item is already ' . ($item->is_active ? 'active' : 'inactive'));
        }

        $item->is_active = $desiredStatus;
        $item->save();

        return $item;
    }
}
