<?php

namespace App\Enums;

use App\Enums\Traits\WithValues;

/**
 * Class ItemStatus
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Enums
 */
enum ItemStatus: string
{
    use WithValues;

    case NEW = 'NEW';
    case UPDATED = 'UPDATED';
}
