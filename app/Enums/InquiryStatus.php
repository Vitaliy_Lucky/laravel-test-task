<?php

namespace App\Enums;

use App\Enums\Traits\WithValues;

/**
 * Class InquiryStatus
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Enums
 */
enum InquiryStatus: string
{
    use WithValues;

    case ACTIVE = 'ACTIVE';
    case PROCESSED = 'PROCESSED';
    case FAILED = 'FAILED';
}
