<?php

namespace App\Models;

use App\Enums\ItemStatus;
use App\Models\Traits\HasEnumStatus;
use Illuminate\Database\Eloquent\{Builder, Factories\HasFactory, Model};

/**
 * Class Item
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Models
 *
 * @property ItemStatus $status
 * @method Builder active()
 * @method Builder inactive()
 */
class Item extends Model
{
    use HasFactory, HasEnumStatus;

    /**
     * @var string
     */
    public $table = 'demo_test';

    /**
     * @var string
     */
    protected string $enumStatus = ItemStatus::class;

    protected $fillable = [
        'ref',
        'name',
        'description',
        'status',
        'is_active',
    ];


    /**
     * @param  Builder  $query
     *
     * @return Builder
     */
    public static function scopeActive(Builder $query): Builder
    {
        return $query->where('is_active', true);
    }


    /**
     * @param  Builder  $query
     *
     * @return Builder
     */
    public static function scopeInactive(Builder $query): Builder
    {
        return $query->where('is_active', false);
    }


    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'ref';
    }
}
