<?php

namespace App\Models\Traits;

use App\Enums\InquiryStatus;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait HasEnumStatus
{
    public function status(): Attribute
    {
        return new Attribute(
            get: fn($value) => $value ? $this->enumStatus::from($value) : null,
            set: fn(\UnitEnum|string $value) => \is_string($value)
                ? $this->enumStatus::from($value)?->value
                : $value->value,
        );
    }
}
