<?php

namespace App\Models;

use App\Enums\InquiryStatus;
use App\Models\Traits\HasEnumStatus;
use Illuminate\Bus\Batch;
use Illuminate\Database\Eloquent\Casts\AsCollection;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Inquiry
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Models
 *
 * @property Collection $payload
 */
class Inquiry extends Model
{
    use HasFactory, HasEnumStatus;

    /**
     * @var string
     */
    public $table = 'demo_test_inquiry';

    /**
     * @var string[]
     */
    protected $fillable = [
        'items_failed_count',
        'items_total_count',
        'items_processed_count',
        'payload',
        'status',
    ];

    protected $casts = [
        'payload' => AsCollection::class,
        'status' => 'string',
    ];

    /**
     * @var string
     */
    protected string $enumStatus = InquiryStatus::class;


    /**
     * @return $this
     */
    public function updateStatus(): Inquiry
    {
        // is the final status which doesn't change - do nothing
        if (\in_array($this->status, [InquiryStatus::PROCESSED, InquiryStatus::FAILED])) {
             return $this;
        }

        if ($this->items_failed_count) {
            $this->status = InquiryStatus::FAILED;
        }

        if ($this->items_processed_count === $this->items_total_count) {
            $this->status = InquiryStatus::PROCESSED;
        }

        return $this;
    }


    /**
     * @param  Batch  $batch
     *
     * @return void
     */
    public function syncWithBatch(Batch $batch): self
    {
        $batch->fresh();
        $this->items_failed_count = $batch->failedJobs;
        $this->items_processed_count = $batch->processedJobs();
        $this->items_total_count = $batch->totalJobs;

        $this->save();

        return $this;
    }
}
