
## Test task with requirements

Not sure is that a sensitive for client in

## Local deployment and test
Requirements:

- docker engine with docker compose
- bash

Clone git repository:
    
    git clone git@gitlab.com:Vitaliy_Lucky/laravel-test-task.git

    cd laravel-test-task
    
Root directory contains the bash script for local deployment, to execute it add +x permission to this file:

    chmod +x ./bootstrap.sh

Execute bootstrap script: 

    ./bootstrap.sh
    

After the bootstrap execution complete the application will be available for test using path: 
<a href="http://10.3.0.1">http://10.3.0.1</a> 




## Run tests
You could run the tests after the application bootstrap

    docker compose exec app php artisan test
